const Discord = require('discord.js');
const readline = require('readline');

const client = new Discord.Client();

var msgch = 'default channel id here';



const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("ready", function(){
  console.log('+--------------------------------------------------+\n|Welcome to the general discord bot interface!     |\n|Here are some commands you can use in the console.|\n|msg  - sends a message to a channel               |\n|list - lists all servers and their channels       |\n|cch  - changes channel for messages               |\n+--------------------------------------------------+');

  rl.on('line', (input) => {
    if (input == "msg") {
      var msg_out = rl.question('Message: ', answer)
        
      function answer(msg_out) {
        client.channels.get(msgch).send(msg_out)
        //console.log(msg_out)
        process.stdout.write('Command line:');
      }
    } 


    else if (input =="list") {
      console.log("Servers:");
        client.guilds.forEach((guild) => {
        console.log(" - " + guild.name)

      
      guild.channels.forEach((channel) => {
          console.log(` -- ${channel.name} (${channel.type}) - ${channel.id}`)
        });
      });
      process.stdout.write('Command line:');
    } 
    
    
    else if (input =="cch") {
      chch = rl.question('Channel ID: ', answer)

      function answer(chch) {
        msgch = chch
        console.log('Current channel: ', msgch)
      }
      
    } else {
      console.log('Enter a valid command!');
      process.stdout.write('Command line:');
    return;
  }});
  process.stdout.write('Command line:');
});


client.login('your token here');